import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';
import { APIResponse } from './models/apiResponse';
import { Folder } from './models/folder';
import { ModuleFolderAccess } from './models/moduleFolderAccess';
import { SnagList } from './module-snag-scrutineer/module-snag-scrutineer.component';
import { stringify } from 'querystring';
import { Customer } from './models/customer';
import { LoginRequest } from './models/LoginRequest';
import { LoginResponse } from './models/loginResponse';

@Injectable()

export class BackendServiceService {

  private appSettings = 'assets/app-settings.json';
  private serverAPI: string;

  constructor(private http: HttpClient) {
  }

  private getServerAPI() {
    return this.http.get(this.appSettings).map((response: Response) => response.json());
  }

  login(loginRequest: LoginRequest): Observable<LoginResponse> {
    var apiResponse = this.http.post<LoginResponse>("http://localhost:59362/api/login/login", loginRequest);
    return apiResponse;
  }

  getCustomer(customerId: number): Observable<APIResponse> {
    var apiResponse = this.http.get<APIResponse>("http://localhost:59362/api/customer/getcustomer?customerid=" + customerId, { headers: this.setHeaders() });
    return apiResponse;
  }

  addEditCustomer(customer: Customer): Observable<APIResponse> {
    var apiResponse = this.http.post<APIResponse>("http://localhost:59362/api/customer/addeditcustomer", customer, { headers: this.setHeaders() });
    return apiResponse;
  }

  deactivateCustomer(customerId: number): Observable<APIResponse> {
    var apiResponse = this.http.get<APIResponse>("http://localhost:59362/api/customer/deactivatecustomer?customerid=" + customerId, { headers: this.setHeaders() });
    return apiResponse;
  }

  getFolder(folderId: number): Observable<APIResponse> {
    var apiResponse = this.http.get<APIResponse>("http://localhost:59362/api/folder/getfolder?folderid=" + folderId, { headers: this.setHeaders() });
    return apiResponse;
  }

  addEditFolder(folder: Folder): Observable<APIResponse> {
    var apiResponse = this.http.post<APIResponse>("http://localhost:59362/api/folder/addEditFolder", folder, { headers: this.setHeaders() });
    return apiResponse;
  }

  deleteFolder(folderId: number): Observable<APIResponse> {
    var apiResponse = this.http.delete<APIResponse>("http://localhost:59362/api/folder/deletefolder?folderid=" + folderId, { headers: this.setHeaders() });
    return apiResponse;
  }

  getModuleFolderAccess(moduleId: number): Observable<APIResponse> {
    var apiResponse = this.http.get<APIResponse>("http://localhost:59362/api/module/getmodulefolderaccess?moduleid=" + moduleId, { headers: this.setHeaders() });
    return apiResponse;
  }

  addModuleFolderAccess(moduleFolderAccess: ModuleFolderAccess): Observable<APIResponse> {
    var apiResponse = this.http.post<APIResponse>("http://localhost:59362/api/module/addmodulefolderaccess", moduleFolderAccess, { headers: this.setHeaders() });
    return apiResponse;
  }

  deleteModuleFolderAccess(moduleFolderAccess: ModuleFolderAccess): Observable<APIResponse> {
    var apiResponse = this.http.post<APIResponse>("http://localhost:59362/api/module/deletemodulefolderaccess", moduleFolderAccess, { headers: this.setHeaders() });
    return apiResponse;
  }

  submitSnagList(snagList: SnagList): Observable<APIResponse> {
    var apiResponse = this.http.post<APIResponse>("http://localhost:59362/api/snagscrutineer/submitsnaglist", snagList, { headers: this.setHeaders() });
    return apiResponse;
  }

  getCookieValue(cookieName: string) {
    var cookieValue = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
    return cookieValue ? cookieValue.pop() : '';
  }

  setHeaders(): HttpHeaders {
    var token = this.getCookieValue('token');
    return new HttpHeaders().set('Authorization', `bearer ${token}`);
  }
}