export class Customer{
    id: number;
    name: string;
    key: string;
    active: boolean;
}