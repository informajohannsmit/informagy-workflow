import { IndexField } from "./indexField";

export class Folder{
    id: number;
    name: string;
    description: string;
    parentFolderID: number;
    available: boolean;
    customerId: number;
    fields: IndexField[];
}