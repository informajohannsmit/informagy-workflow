export class LoginResponse {
    token: string;
    errorOccurred: boolean;
    errorMessage: string;
}