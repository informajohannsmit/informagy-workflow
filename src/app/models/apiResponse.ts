export interface APIResponse{
    data: any[];
    errorOccurred: boolean;
    errorMessage: string;
}