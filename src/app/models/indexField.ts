export class IndexField{
    id: number;
    name: string;
    dataType: string;
    defaultValue: string;
    minLength: number;
    maxLenght: number;
    minValue: number;
    maxValue: number;
}