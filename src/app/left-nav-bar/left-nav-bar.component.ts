import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  adminOnly: boolean;
}
export const Routes: RouteInfo[] = [
  { path: './system-settings', title: 'System Settings', icon: 'pe-7s-config', class: '', adminOnly: true },
  { path: './customers', title: 'Customers', icon: 'pe-7s-users', class: '', adminOnly: true},
  { path: './folders', title: 'Folders', icon: 'pe-7s-folder', class: '', adminOnly: true },
  { path: '', title: 'Users & Groups', icon: 'pe-7s-users', class: '', adminOnly: true },
  { path: '', title: 'Workflows', icon: 'pe-7s-news-paper', class: '', adminOnly: true },
  { path: './snag-scrutineer', title: 'Snag Scrutineer', icon: 'pe-7s-search', class: '', adminOnly: false },
  { path: './snag-manager', title: 'Snag Manager', icon: 'pe-7s-check', class: '', adminOnly: false}
];

@Component({
  selector: 'INFWF-left-nav-bar',
  templateUrl: './left-nav-bar.component.html',
  styleUrls: ['./left-nav-bar.component.css']
})
export class LeftNavBarComponent extends GlobalBaseComponent implements OnInit {

  menuItems: any[];

  constructor(private router: Router, private backendService: BackendServiceService, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
    this.menuItems = [];
    for (var route of Routes) {
      if (route.adminOnly && !this.isAdminUser)
        continue;

      this.menuItems.push(route);
    }
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
