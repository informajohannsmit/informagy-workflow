import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelFolderComponent } from './admin-panel-folder.component';

describe('AdminPanelFolderComponent', () => {
  let component: AdminPanelFolderComponent;
  let fixture: ComponentFixture<AdminPanelFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPanelFolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
