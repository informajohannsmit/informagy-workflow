import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Folder } from '../models/folder';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'INFWF-admin-panel-folder',
  templateUrl: './admin-panel-folder.component.html',
  styleUrls: ['./admin-panel-folder.component.css']
})
export class AdminPanelFolderComponent extends GlobalBaseComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<string>();

  constructor(private backendService: BackendServiceService, private router: Router, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
    
  }
}
