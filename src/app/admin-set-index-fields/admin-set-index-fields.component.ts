import { Component, OnInit, Input } from '@angular/core';
import { IndexField } from '../models/indexField';

@Component({
  selector: 'INFWF-admin-set-index-fields',
  templateUrl: './admin-set-index-fields.component.html',
  styleUrls: ['./admin-set-index-fields.component.css']
})
export class AdminSetIndexFieldsComponent implements OnInit {

  @Input() fields: IndexField[];

  constructor() { }

  ngOnInit() {
  }

  addField(){
    this.fields.push(new IndexField());
  }

  close(){
    console.log(this.fields);
  }

}
