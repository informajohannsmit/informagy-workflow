import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSetIndexFieldsComponent } from './admin-set-index-fields.component';

describe('AdminSetIndexFieldsComponent', () => {
  let component: AdminSetIndexFieldsComponent;
  let fixture: ComponentFixture<AdminSetIndexFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSetIndexFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSetIndexFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
