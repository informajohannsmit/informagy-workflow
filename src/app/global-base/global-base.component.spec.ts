import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalBaseComponent } from './global-base.component';

describe('GlobalBaseComponent', () => {
  let component: GlobalBaseComponent;
  let fixture: ComponentFixture<GlobalBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
