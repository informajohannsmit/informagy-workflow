import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { BackendServiceService } from '../backend-service.service';
import { Folder } from '../models/folder';
import { Observable } from '../../../node_modules/rxjs';

declare const $: any;

@Component({
  selector: 'INFWF-global-base',
  templateUrl: './global-base.component.html',
  styleUrls: ['./global-base.component.css']
})
export class GlobalBaseComponent implements OnInit {

  isAdminUser: boolean;
  moduleFolderLibrary: Folder[];
  moduleAvailableFolders: Folder[];
  loaderEnabled = false;

  constructor(private _router: Router, private _backendService: BackendServiceService, private _changeDetector: ChangeDetectorRef) {
    this.isAdminUser = (this.getCookieValue('IsAdminUser') == 'true');
  }

  ngOnInit() {
    this.loaderEnabled = JSON.parse(this.getCookieValue("loaderEnabled"));
  }

  enableLoader(){
    this.setCookie("loaderEnabled", "true");
    this.loaderEnabled = JSON.parse(this.getCookieValue("loaderEnabled"));
    this._changeDetector.detectChanges();
    console.log(this.loaderEnabled);
  }

  disableLoader(){
    this.setCookie("loaderEnabled", "false");
    this.loaderEnabled = JSON.parse(this.getCookieValue("loaderEnabled"));
    console.log(this.loaderEnabled);
  }

  setCookie(cookieName: string, cookieValue: string){
    document.cookie = `${cookieName}=${cookieValue}`;
  }

  getCookieValue(cookieName: string) {
    var cookieValue = document.cookie.match('(^|;)\\s*' + cookieName + '\\s*=\\s*([^;]+)');
    return cookieValue ? cookieValue.pop() : '';
  }

  cancelAction() {
    this._router.navigate(['workspace']);
  }

  getModuleFolders(moduleId: number) {
    this._backendService.getFolder(0).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        //handle error here...
      }
      else {
        //Get Module Folder Access
        var allFolders = apiResponse.data;

        this._backendService.getModuleFolderAccess(moduleId).subscribe();
      }
    });
  }

  handleError(errorMessage: string) {
    console.log('ERROR: ' + errorMessage);
    this.showNotification('top', 'right', 'danger', 'ERROR OCCURRED', errorMessage);
  }

  showNotification(from, align, type, header, notificaitonMesage) {

    //from: 'top', 'bottom'
    //align: 'left', 'center', 'right'
    //type: '','info','success','warning','danger'

    $.notify({
      icon: "pe-7s-gift",
      message: "<h3>" + header + "</h3><br> " + notificaitonMesage
    }, {
        type: type,
        timer: 4000,
        placement: {
          from: from,
          align: align
        }
      });
  }
}