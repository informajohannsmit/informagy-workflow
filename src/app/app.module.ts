import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { WorkspaceComponent } from './workspace/workspace.component';
import { AdminPanelSystemSettingsComponent } from './admin-panel-system-settings/admin-panel-system-settings.component';
import { AdminPanelFoldersComponent } from './admin-panel-folders/admin-panel-folders.component';
import { AdminPanelFolderComponent } from './admin-panel-folder/admin-panel-folder.component';
import { AdminSetIndexFieldsComponent } from './admin-set-index-fields/admin-set-index-fields.component';
import { LeftNavBarComponent } from './left-nav-bar/left-nav-bar.component';
import { HeadNavBarComponent } from './head-nav-bar/head-nav-bar.component';
import { ModuleSnagScrutineerComponent } from './module-snag-scrutineer/module-snag-scrutineer.component';
import { GlobalBaseComponent } from './global-base/global-base.component';
import { BackendServiceService } from './backend-service.service';
import { ModuleSnagManagerComponent } from './module-snag-manager/module-snag-manager.component';
import { CustomersComponent } from './customers/customers.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WorkspaceComponent,
    AdminPanelSystemSettingsComponent,
    AdminPanelFoldersComponent,
    AdminPanelFolderComponent,
    AdminSetIndexFieldsComponent,
    LeftNavBarComponent,
    HeadNavBarComponent,
    ModuleSnagScrutineerComponent,
    GlobalBaseComponent,
    ModuleSnagManagerComponent,
    CustomersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    BackendServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
