import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'INFWF-workspace',
  templateUrl: './workspace.component.html'
})
export class WorkspaceComponent extends GlobalBaseComponent implements OnInit {

  constructor(private backendService:BackendServiceService, private router: Router, private changeDetector: ChangeDetectorRef) { 
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
  }

}
