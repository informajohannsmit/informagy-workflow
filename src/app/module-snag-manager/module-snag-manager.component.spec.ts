import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleSnagManagerComponent } from './module-snag-manager.component';

describe('ModuleSnagManagerComponent', () => {
  let component: ModuleSnagManagerComponent;
  let fixture: ComponentFixture<ModuleSnagManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleSnagManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleSnagManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
