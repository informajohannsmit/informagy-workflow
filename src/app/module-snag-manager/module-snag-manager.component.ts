import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { Router } from '@angular/router';
import { BackendServiceService } from '../backend-service.service';

@Component({
  selector: 'INFWF-module-snag-manager',
  templateUrl: './module-snag-manager.component.html',
  styleUrls: ['./module-snag-manager.component.css']
})
export class ModuleSnagManagerComponent extends GlobalBaseComponent implements OnInit {

  constructor(router: Router, backendService: BackendServiceService, private changeDetector: ChangeDetectorRef) 
  {
    super(router, backendService, changeDetector);
   }

  ngOnInit() {
  }

}
