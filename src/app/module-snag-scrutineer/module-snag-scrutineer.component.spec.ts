import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleSnagScrutineerComponent } from './module-snag-scrutineer.component';

describe('ModuleSnagScrutineerComponent', () => {
  let component: ModuleSnagScrutineerComponent;
  let fixture: ComponentFixture<ModuleSnagScrutineerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleSnagScrutineerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleSnagScrutineerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
