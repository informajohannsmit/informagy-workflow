import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';
import { Folder } from '../models/folder';
import { Module } from '../models/module';
import { ModuleFolderAccess } from '../models/moduleFolderAccess';

export class SnagItem {
  ID: number;
  DateTime: string;
  Area: string;
  Description: string;
  Image: string;
}

export class SnagList {
  ID: number;
  ProjectID: number;
  Items: SnagItem[];
}

const moduleID = 1;

@Component({
  selector: 'INFWF-module-snag-scrutineer',
  templateUrl: './module-snag-scrutineer.component.html',
  styleUrls: ['./module-snag-scrutineer.component.css']
})

export class ModuleSnagScrutineerComponent extends GlobalBaseComponent implements OnInit {

  folders: Folder[];
  availableFolders: Folder[];
  module: Module;
  snagList: SnagList;
  newSnag: SnagItem;
  isProjectSelected: boolean;
  snagArea: string;
  snagImage: string;
  emailReceipient: string = "johann.smit@informagy.com";

  constructor(private router: Router, private backendService: BackendServiceService, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
    this.snagList = new SnagList();
    this.newSnag = new SnagItem();
  }

  ngOnInit() {
    this.loadFolders();
  }

  loadFolders() {
    this.backendService.getModuleFolderAccess(moduleID).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.folders = apiResponse.data;
        this.availableFolders = this.folders.filter(folder => folder.available == true);
        console.log('loading complete');
      }
    });
  }

  makeAvailable(folder: Folder) {
    let moduleFolder: ModuleFolderAccess = new ModuleFolderAccess();
    moduleFolder.folderId = folder.id;
    moduleFolder.moduleId = moduleID;

    this.backendService.addModuleFolderAccess(moduleFolder).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.loadFolders();
      }
    });
  }

  returnToLibrary(folder: Folder) {
    let moduleFolder: ModuleFolderAccess = new ModuleFolderAccess();
    moduleFolder.folderId = folder.id;
    moduleFolder.moduleId = moduleID;

    this.backendService.deleteModuleFolderAccess(moduleFolder).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.loadFolders();
      }
    });
  }

  //-----------------------------------------------------------------------------------------
  // User Portal
  //-----------------------------------------------------------------------------------------

  selectProject() {
    this.isProjectSelected = true;
  }

  submitSnagList() {
    this.backendService.submitSnagList(this.snagList).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.showNotification("top", "right", "success", "Sucess", "success")
        this.router.navigate(["workspace"]);
      }
    });
  }

  setSnagImage(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      reader.readAsDataURL(event.target.files[0]);
      setTimeout(()=>{ 
        this.snagImage = reader.result.toString(); 
        console.log(this.snagImage);
      }, 1000);
    }
    
  }

  addToSnagList() {
    if (this.snagList.Items == null)
      this.snagList.Items = [];

    this.newSnag.ID = this.snagList.Items.length + 1;
    this.newSnag.DateTime = new Date().toISOString();
    this.newSnag.Area = this.snagArea;
    this.newSnag.Image = this.snagImage;
    this.snagList.Items.push(this.newSnag);
    this.newSnag = new SnagItem();
  }

  removeSnagItem(snag: SnagItem){
    this.snagList.Items.splice(this.snagList.Items.findIndex(item => item.ID == snag.ID), 1);
  }
}