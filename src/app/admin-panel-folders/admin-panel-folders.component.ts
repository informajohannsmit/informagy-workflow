import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Folder } from '../models/folder';
import { BackendServiceService } from '../backend-service.service';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { Router } from '@angular/router';
import { Customer } from '../models/customer';
import { IndexField } from '../models/indexField';

@Component({
  selector: 'INFWF-admin-panel-folders',
  templateUrl: './admin-panel-folders.component.html',
  styleUrls: ['./admin-panel-folders.component.css']
})
export class AdminPanelFoldersComponent extends GlobalBaseComponent implements OnInit {

  customers: Customer[] = [];
  folders: Folder[] = [];
  inProgressFolder: Folder;
  addEditFolderFlag: Boolean;
  setIndexFieldsFlag: Boolean;

  constructor(private backendService: BackendServiceService, private router: Router, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
    this.loadCustomers();
    this.loadFolders();
    console.log(this.customers);
  }

  loadCustomers() {
    this.backendService.getCustomer(0).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.customers = apiResponse.data;
      }
    });
  }

  loadFolders() {
    this.backendService.getFolder(0).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.folders = apiResponse.data;
      }
    });
  }

  addEditFolder() {
    console.log(this.folders);
    this.inProgressFolder = new Folder();
    this.inProgressFolder.id = 0;
    this.addEditFolderFlag = true;
  }

  saveFolder() {
    this.addEditFolderFlag = false;
    this.backendService.addEditFolder(this.inProgressFolder).subscribe(apiResponse => {

      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.loadFolders();
      }
    });
  }

  removeFolder(folderId: number) {
    this.backendService.deleteFolder(folderId).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        console.log(apiResponse);
        this.loadFolders();
      }
    });
  }

  setIndexFields(folder: Folder) {
    this.inProgressFolder = folder;

    if (this.inProgressFolder.fields == null) {
      this.inProgressFolder.fields = []
      this.inProgressFolder.fields.push(new IndexField());
    }

    this.setIndexFieldsFlag = true;
  }

  addIndexField() {
    this.inProgressFolder.fields.push(new IndexField());
  }

  cancelSetIndexFields(){
    this.setIndexFieldsFlag = false;
    this.inProgressFolder = null;
  }
}
