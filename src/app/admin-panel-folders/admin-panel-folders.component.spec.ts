import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelFoldersComponent } from './admin-panel-folders.component';

describe('AdminPanelFoldersComponent', () => {
  let component: AdminPanelFoldersComponent;
  let fixture: ComponentFixture<AdminPanelFoldersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPanelFoldersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
