import { Component, OnInit, Injectable, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';
import { LoginRequest } from '../models/LoginRequest';
// import { BackendServiceService } from '../backend-service.service';

@Component({
  selector: 'INFWF-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends GlobalBaseComponent implements OnInit {

  adminPanelLogin: boolean = false;
  loginRequest: LoginRequest;

  constructor(private router: Router, private backendService: BackendServiceService, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
    this.loginRequest = new LoginRequest();
  }

  submitLogin() {

    this.backendService.login(this.loginRequest).subscribe(apiResponse => {

      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.setCookie("token", apiResponse.token);
        
        if (this.adminPanelLogin) {
          document.cookie = "IsAdminUser=true;";
        }
        else {
          document.cookie = "IsAdminUser=false;";
        }

        this.router.navigate(['workspace']);
      }
    });
  }
}