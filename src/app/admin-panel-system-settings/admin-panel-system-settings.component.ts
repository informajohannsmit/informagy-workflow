import { Component, OnInit } from '@angular/core';
import { systemValue } from '../models/systemValue';

@Component({
  selector: 'INFWF-admin-panel-system-settings',
  templateUrl: './admin-panel-system-settings.component.html',
  styleUrls: ['./admin-panel-system-settings.component.css']
})
export class AdminPanelSystemSettingsComponent implements OnInit {

  systemValues: systemValue[];

  constructor() { }

  ngOnInit() {
    if(this.systemValues == null){
      this.systemValues = [];
    }
  }

  addSystemValue(){
    this.systemValues.push(new systemValue());
  }
}
