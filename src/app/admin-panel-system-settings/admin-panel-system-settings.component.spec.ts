import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPanelSystemSettingsComponent } from './admin-panel-system-settings.component';

describe('AdminPanelSystemSettingsComponent', () => {
  let component: AdminPanelSystemSettingsComponent;
  let fixture: ComponentFixture<AdminPanelSystemSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPanelSystemSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPanelSystemSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
