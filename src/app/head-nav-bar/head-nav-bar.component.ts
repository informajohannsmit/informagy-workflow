import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy, getLocaleCurrencyName } from '@angular/common';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { Routes } from '../left-nav-bar/left-nav-bar.component';
import { BackendServiceService } from '../backend-service.service';

@Component({
    selector: 'INFWF-head-nav-bar',
    templateUrl: './head-nav-bar.component.html',
    styleUrls: ['./head-nav-bar.component.css']
})
export class HeadNavBarComponent extends GlobalBaseComponent implements OnInit {

    private listTitles: any[];
    location: Location;
    private toggleButton: any;
    private sidebarVisible: boolean;

    constructor(private router: Router, private backendService: BackendServiceService, location: Location, private element: ElementRef, private changeDetector: ChangeDetectorRef) {
        super(router, backendService, changeDetector);
        this.location = location;
        this.sidebarVisible = false;
    }

    ngOnInit() {
        this.listTitles = Routes.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    getTitle() {

        if (this.isAdminUser)
            return 'Administration Panel';
        else
            return 'User Portal';
        // var titlee = this.location.prepareExternalUrl(this.location.path());
        // titlee = titlee.split('/').pop();
        // for(var item = 0; item < this.listTitles.length; item++){
        //     if(this.listTitles[item].path === titlee){
        //         return this.listTitles[item].title;
        //     }
        // }
        // return 'Dashboard';
    }

}
