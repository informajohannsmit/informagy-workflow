import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalBaseComponent } from '../global-base/global-base.component';
import { BackendServiceService } from '../backend-service.service';
import { Router } from '@angular/router';
import { Customer } from '../models/customer';

@Component({
  selector: 'INFWF-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent extends GlobalBaseComponent implements OnInit {

  customers: Customer[] = [];
  inProgressCustomer: Customer;
  addEditCustomerFlag: Boolean;

  constructor(private backendService: BackendServiceService, private router: Router, private changeDetector: ChangeDetectorRef) {
    super(router, backendService, changeDetector);
  }

  ngOnInit() {
    this.loadCustomers();
    console.log(this.customers);
  }

  loadCustomers() {
    this.enableLoader();
    this.backendService.getCustomer(0).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.customers = apiResponse.data;
      }
      this.disableLoader();
    });
  }

  addEditCustomer(customerId: number) {
    this.enableLoader();
    if (customerId == null) {
      this.inProgressCustomer = new Customer();
      this.inProgressCustomer.id = 0;
    }
    else {
      // this.inProgressCustomer = customerId;
    }

    this.addEditCustomerFlag = true;
    this.disableLoader();
  }

  saveCustomer() {
    this.enableLoader();
    this.addEditCustomerFlag = false;
    this.backendService.addEditCustomer(this.inProgressCustomer).subscribe(apiResponse => {
      if (apiResponse.errorOccurred) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.loadCustomers();
      }
      this.disableLoader();
    });
    
  }

  deactivateCustomer(customerId: number) {
    this.enableLoader();
    this.backendService.deactivateCustomer(customerId).subscribe(apiResponse => {
      if (apiResponse.errorMessage) {
        this.handleError(apiResponse.errorMessage);
      }
      else {
        this.loadCustomers();
      }
    });
    this.disableLoader();
  }
}