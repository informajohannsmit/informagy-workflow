import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Custom components
import {
  LoginComponent,
  WorkspaceComponent,
  AdminPanelSystemSettingsComponent,
  AdminPanelFoldersComponent
} from './component-index';
import { ModuleSnagScrutineerComponent } from './module-snag-scrutineer/module-snag-scrutineer.component';
import { ModuleSnagManagerComponent } from './module-snag-manager/module-snag-manager.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'workspace', component: WorkspaceComponent, children: [
      { path: 'system-settings', component: AdminPanelSystemSettingsComponent },
      { path: 'customers', component: CustomersComponent},
      { path: 'folders', component: AdminPanelFoldersComponent },
      { path: 'snag-scrutineer', component: ModuleSnagScrutineerComponent},
      { path: 'snag-manager', component: ModuleSnagManagerComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
