USE InformagyWorkflow
GO

CREATE PROCEDURE AddEditSystemValue @SystemValueID INT, @Key VARCHAR(50), @Value VARCHAR(50), @UserDefined BIT
AS
	IF (@SystemValueID = 0)
		BEGIN
			INSERT INTO SystemValue ([Key], [Value], [UserDefined])
			VALUES (@Key, @Value, @UserDefined)
		END	
	ELSE
		BEGIN
			UPDATE SystemValue 
			SET [Key] = @Key,
				[Value] = @Value
		END