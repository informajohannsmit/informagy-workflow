USE InformagyWorkflow
GO

CREATE PROCEDURE GetSystemUser @Username VARCHAR(21)
AS
(
	SELECT * 
	FROM SystemUser 
	WHERE Username = @Username
)