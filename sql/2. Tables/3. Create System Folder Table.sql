USE InformagyWorkflow
GO
CREATE TABLE [SystemFolder](
	ID INT IDENTITY PRIMARY KEY NOT NULL,
	Name VARCHAR(50) NOT NULL,
	EditMode BIT NOT NULL DEFAULT 0,
	EditedBy INT
)

ALTER TABLE [SystemFolder]
ADD FOREIGN KEY (EditedBy) REFERENCES [SystemUser](ID);