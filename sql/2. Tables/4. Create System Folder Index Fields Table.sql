USE InformagyWorkflow
GO
CREATE TABLE [SystemFolderIndexFields](
	ID INT IDENTITY PRIMARY KEY NOT NULL,
	FolderID INT NOT NULL,
	FieldName VARCHAR(50) NOT NULL,
	DisplayName VARCHAR(50) NOT NULL,
	Type VARCHAR(10) NOT NULL,
	MinimumValue DECIMAL(18, 10),
	MaximumValue DECIMAL(18, 10),
	MinimumLength INT,
	MaximumLength INT,
	DefaultValue VARCHAR(50),
	DecimalLength INT
)

ALTER TABLE [SystemFolderIndexFields]
ADD FOREIGN KEY (FolderID) REFERENCES [SystemFolder](ID);