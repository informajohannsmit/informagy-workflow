USE InformagyWorkflow
GO

CREATE TABLE SystemUserRole(
	ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	RoleName VARCHAR(50) NOT NULL UNIQUE,
	Removable  BIT NOT NULL DEFAULT 1
)

INSERT INTO SystemUserRole (RoleName, Removable)
VALUES ('System Administrator', 0)