USE InformagyWorkflow
GO
CREATE TABLE [SystemUser](
	ID INT IDENTITY PRIMARY KEY NOT NULL,
	FirstName VARCHAR(20) NOT NULL,
	LastName VARCHAR(20) NOT NULL,
	Username VARCHAR(21) NOT NULL,
	EmailAddress VARCHAR(60) NOT NULL,
	Password VARCHAR(20) NOT NULL,
	Locked BIT NOT NULL DEFAULT 0,
	ActiveDirectoryUser BIT NOT NULL DEFAULT 0
)

ALTER TABLE [SystemUser]
ADD CONSTRAINT UC_Username UNIQUE (Username);

ALTER TABLE [SystemUser]
ADD CONSTRAINT UC_EmailAddress UNIQUE (EmailAddress);